import React from "react";
import Head from "next/head";
import dynamic from "next/dynamic";
import Header from "../components/header";

const DynamicLazyComponent = dynamic(() => import("../components/post"));

const Post = () => {
  return (
    <>
      <Head>
        <title>Blog</title>
        <meta name="description" content="Crystal World Blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main className="bg-black min-h-screen p-2 max-w-1024 m-auto">
        <h1 className="text-white text-2xl font-bold py-5 cursive">
          Post Lists
        </h1>
        <section className="grid md:grid-cols-3 gap-8">
          <DynamicLazyComponent />
        </section>
      </main>
    </>
  );
};

export default Post;
