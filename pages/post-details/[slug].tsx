import React, { useState, useEffect } from "react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import sanityClient from "../../client";
import Header from "../../components/header";

const PostDetails = () => {
  const router = useRouter();
  const { slug } = router.query;
  const [postDetailData, setPostDetail] = useState<any>(null);
  useEffect(() => {
    sanityClient
      .fetch(
        `*[slug.current == '${slug}']{
        title,
        _id,
        slug,
        excerpt,
        mainImage{
          asset->{
            _id,
            url
          },
        alt
      },
      body
    }`
      )
      .then((data) => setPostDetail(data[0]))
      .catch(console.error);
  }, [slug]);

  return (
    <>
      <Head>
        <title>{postDetailData?.title}</title>
        <meta name="description" content={postDetailData?.excerpt} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main className="bg-black min-h-screen p-2 max-w-1024 m-auto">
        {!postDetailData && (
          <h2 className="text-white text-lg py-5 cursive align-center">
            Loading....
          </h2>
        )}
        {postDetailData && (
          <article key={postDetailData?.slug?.current}>
            <h2 className="text-white text-lg font-bold py-5 cursive">
              {postDetailData?.title}
            </h2>
            <span style={{ height: 300 }}>
              <Image
                loading="lazy"
                src={postDetailData?.mainImage?.asset?.url}
                alt={postDetailData?.mainImage?.alt}
                width={500}
                height={300}
                layout="responsive"
              />
            </span>

            <p className="text-white text-sm py-5 text-justify">
              {postDetailData?.body[0]?.children[0]?.text}
            </p>
          </article>
        )}
      </main>
    </>
  );
};

export default PostDetails;
