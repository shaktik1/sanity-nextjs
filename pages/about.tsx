import React, { useState, useEffect } from "react";
import Head from "next/head";
import sanityClient from "../client";
import Header from "../components/header";
import Image from "next/image";

const About = () => {
  const [pageData, setPageData] = useState<any>(null);

  useEffect(() => {
    sanityClient
      .fetch(
        `*[slug.current == 'about']{
        name,
        _id,
        slug,
        excerpt,
        mainImage{
          asset->{
            _id,
            url
          },
        alt
      },
      body
    }`
      )
      .then((data) => setPageData(data[0]))
      .catch(console.error);
  }, []);

  return (
    <>
      <Head>
        <title>{pageData?.name}</title>
        <meta name="description" content={pageData?.excerpt} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main className="bg-black min-h-screen p-2 max-w-1024 m-auto">
        {pageData && (
          <>
            <h1 className="text-white text-2xl font-bold py-5 cursive">
              {pageData?.name}
            </h1>
            <p className="text-white text-sm py-1 text-justify">
              <span className="float-left mr-10 mb-5">
                <Image
                  loading="lazy"
                  src={pageData?.mainImage?.asset?.url}
                  alt={pageData?.mainImage?.alt}
                  height={230}
                  width={230}
                />
              </span>
              {pageData?.excerpt}
            </p>
            <span
              dangerouslySetInnerHTML={{
                __html: pageData?.body[0]?.children[0]?.text,
              }}
            ></span>
          </>
        )}
      </main>
    </>
  );
};

export default About;
