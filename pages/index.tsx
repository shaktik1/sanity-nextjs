import React, { useState, useEffect } from "react";
import Head from "next/head";
import sanityClient from "../client";
import Header from "../components/header";

const Home = () => {
  const [pageData, setPageData] = useState<any>(null);

  useEffect(() => {
    sanityClient
      .fetch(
        `*[slug.current == 'home']{
        name,
        _id,
        slug,
        excerpt,
        mainImage{
          asset->{
            _id,
            url
          },
        alt
      },
      body
    }`
      )
      .then((data) => setPageData(data[0]))
      .catch(console.error);
  }, []);

  return (
    <main className="bg-gray-800 min-h-screen">
      <Head>
        <title>{pageData?.name}</title>
        <meta name="description" content={pageData?.excerpt} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      {pageData && (
        <>
          <img
            src={pageData?.mainImage?.asset?.url}
            alt={pageData?.mainImage?.alt}
            className="absolute object-cover w-full h-full"
          />
          <section className="relative justify-center min-h-screen pt-12 lg:pt-60 px-10 max-w-1024 m-auto">
            <h1 className="text-white text-bold text-center cursive my-10 lg:text-25xl text-4xl full">
              {pageData?.excerpt}
            </h1>
            <h3 className="text-white text-bold text-center cursive lg:text-15xl text-2xl full">
              {pageData?.body[0]?.children[0]?.text}
            </h3>
          </section>
        </>
      )}
    </main>
  );
};

export default Home;
