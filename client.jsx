import sanityClient from "@sanity/client";

export default sanityClient({
  projectId: "umaeatpl",
  dataset: "production",
});
