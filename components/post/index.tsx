import React, { useState, useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import sanityClient from "../../client";

const Post = () => {
  const [postData, setPost] = useState<any[]>([]);
  useEffect(() => {
    sanityClient
      .fetch(
        `*[_type == 'post']{
      title,
      slug,
      excerpt,
      mainImage{
        asset->{
          _id,
          url
        },
        alt
      }
    }`
      )
      .then((data) => setPost(data))
      .catch(console.error);
  }, []);
  console.log("postData", postData);
  return (
    <>
      {postData.length <= 0 && (
        <h2 className="text-white text-lg py-5 cursive align-center">
          Loading....
        </h2>
      )}
      {postData &&
        postData.map((post, index) => (
          <article key={index}>
            <Link href={"/post-details/" + post?.slug?.current} passHref>
              <h2 className="text-white text-lg font-bold py-5 cursive cursor-pointer">
                {post?.title}
              </h2>
            </Link>
            <Link href={"/post-details/" + post?.slug?.current} passHref>
              <span className="cursor-pointer">
                <Image
                  loading="lazy"
                  src={post?.mainImage?.asset?.url}
                  alt={post?.mainImage?.alt}
                  width={500}
                  height={500}
                />
              </span>
            </Link>
            <h3 className="text-white text-lg py-5">{post?.excerpt}</h3>
          </article>
        ))}
    </>
  );
};

export default Post;
