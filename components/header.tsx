import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { SocialIcon } from "react-social-icons";

const Header = () => {
  const router = useRouter();
  const activePath = router.pathname;
  return (
    <header className="bg-gray-800 relative content-center">
      <div className="container mx-auto flex justify-between max-w-1024">
        <nav className="flex">
          <Link href="/">
            <a
              className={
                (activePath == "/" ? "bg-red-800 " : "") +
                "inline-flex text-white text-5xl item-center py-2 px-2 md:mr-4 mr-2 cursive hover:bg-red-800"
              }
            >
              Crystal
            </a>
          </Link>
          <Link href="/about">
            <a
              className={
                (activePath == "/about" ? "bg-red-800 " : "") +
                "inline-flex text-white text-3xl item-center py-4 md:px-2 px-1 md:mr-4 mr-2 cursive hover:bg-red-800"
              }
            >
              About
            </a>
          </Link>
          <Link href="/post">
            <a
              className={
                (activePath == "/post" ? "bg-red-800 " : "") +
                "inline-flex text-white text-3xl item-center py-4 md:px-2 px-1 md:mr-4 mr-2 cursive hover:bg-red-800"
              }
            >
              Post
            </a>
          </Link>
        </nav>
        <div className="inline-flex py-3 px-1 my-2">
          <SocialIcon
            url="https://twitter.com/"
            className="md:mr-4 mr-2"
            target="_blank"
            fgColor="#fff"
            style={{ width: 30, height: 30 }}
          />
          <SocialIcon
            url="https://facebook.com/"
            className="md:mr-4 mr-2"
            target="_blank"
            fgColor="#fff"
            style={{ width: 30, height: 30 }}
          />
        </div>
      </div>
    </header>
  );
};

export default Header;
